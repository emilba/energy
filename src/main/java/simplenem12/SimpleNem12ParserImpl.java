package simplenem12;

import org.apache.commons.lang.Validate;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class SimpleNem12ParserImpl implements SimpleNem12Parser {

  @Override
  public Collection<MeterRead> parseSimpleNem12(File file) throws IOException {
    return this.parse(Files.lines(Paths.get(file.getPath())));
  }

  Set<MeterRead> parse(Stream<String> stream) {
    Stack<MeterRead> stack = new Stack<>();
    
    Set<MeterRead> reads = stream.skip(1)
        .map(reading -> this.map(reading, stack))
        .filter(mr -> mr != null)
        .collect(toSet());
    return reads;
  }

  private MeterRead map(String reading, Stack<MeterRead> stack) {
    if (reading.startsWith("200,")) {
      stack.clear();
      MeterRead mr = MeterRead.fromString(reading);
      stack.push(mr);
      return mr;
    }

    if (reading.startsWith("300,")) {
      MeterRead mr = stack.peek();
      this.addVolumeReading(mr, reading);
    }
    
    return null;
  }

  private void addVolumeReading(MeterRead mr, String volumeReading) {
    Validate.isTrue(volumeReading.startsWith("300,"));
    Validate.notNull(mr);
    
    String[] strArr = volumeReading.split(",");
    String dateStr = strArr[1];
    String volumeStr = strArr[2];
    String qualityStr = strArr[3];
    
    LocalDate localDate = LocalDate.parse(dateStr, DateTimeFormatter.BASIC_ISO_DATE);
    MeterVolume meterVolume = new MeterVolume(new BigDecimal(volumeStr), Quality.valueOf(qualityStr));
    
    mr.appendVolume(localDate, meterVolume);
    
  }
}
