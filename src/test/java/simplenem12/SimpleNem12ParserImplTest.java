package simplenem12;

import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Set;
import java.util.SortedMap;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@Test
public class SimpleNem12ParserImplTest {

  private SimpleNem12ParserImpl parser = new SimpleNem12ParserImpl();
  
  public void parseStartRecordShouldReturnEmptyCollection() {
    Stream<String> stream = Stream.of("100");
    Set<MeterRead> reads = parser.parse(stream);
    assertThat(reads, empty());
  }
  
  public void parse200ReadingShouldReturnEmptyMeterRead() {
    Stream<String> stream = Stream.of("100","200,6123456789,KWH");
    Set<MeterRead> reads = parser.parse(stream);
    
    assertThat(reads, hasSize(1));
    
    MeterRead read = reads.iterator().next();
    assertThat(read.getVolumes().keySet(), empty());
  }
  
  public void parse200And300ReadingShouldCreateSingleMeterRead() {
    Stream<String> stream = Stream.of("100","200,6123456789,KWH", "300,20161113,-50.8,A");
    Set<MeterRead> reads = parser.parse(stream);
    
    assertThat(reads, hasSize(1));
    MeterRead read = reads.iterator().next();
    
    SortedMap<LocalDate, MeterVolume> volumes = read.getVolumes();
    
    assertThat(read.getVolumes().keySet(), hasSize(1));
    
    assertThat(volumes.keySet().iterator().next(), equalTo(LocalDate.parse("20161113", DateTimeFormatter.BASIC_ISO_DATE)));
    
    MeterVolume volume = volumes.values().iterator().next();
    assertThat(volume.getVolume(), equalTo(new BigDecimal("-50.8")));
    assertThat(volume.getQuality(), equalTo(Quality.A));
  }
  
  public void testHarnessTest() throws IOException {
    String path = getClass().getResource("/SimpleNem12.csv").getFile();
    File file = new File(path);
    
    assertThat(file.exists(), equalTo(true));
    
    Collection<MeterRead> meterReads = parser.parseSimpleNem12(file);
    
    MeterRead read6123456789 = meterReads.stream().filter(mr -> mr.getNmi().equals("6123456789")).findFirst().get();
    System.out.println(String.format("Total volume for NMI 6123456789 is %f", read6123456789.getTotalVolume()));  // Should be -36.84
    assertThat(read6123456789.getTotalVolume(), equalTo(new BigDecimal("-36.84")));

    MeterRead read6987654321 = meterReads.stream().filter(mr -> mr.getNmi().equals("6987654321")).findFirst().get();
    System.out.println(String.format("Total volume for NMI 6987654321 is %f", read6987654321.getTotalVolume()));  // Should be 14.33
    assertThat(read6987654321.getTotalVolume(), equalTo(new BigDecimal("14.33")));
  }
  
}
